$(document).ready(function () {
    var isMobile = false;
    if ($(window).width() < 1200) {
        isMobile = true;
    }

    /********** Слайдер портфолио **********/
    var opacity = 0.1;
    var numberMarginBottom = 0;
    var portfolioSlider = {
        offset: 0,
        slideDefaultWidth: 280,
        slideWidth: 280,
        slideMaxWidth: 470,
        slidesCount: 9,
        speed: 4,
        textMargin: 0,
        textOpaity: 0
    }

    function scrollLeft() {
        if (portfolioSlider.slideWidth == portfolioSlider.slideDefaultWidth) {
            const prevSlide = $('.js-portfolio-card.active').prev();
            //Проверка на то, есть ли впереди ещё слайды
            if (prevSlide.hasClass('js-portfolio-card')) {
                portfolioSlider.slideWidth = portfolioSlider.slideMaxWidth;
                opacity = 0.9;
                $('.js-portfolio-card.active').removeClass('active');
                prevSlide.addClass('active');
                portfolioSlider.textMargin = -70;
                portfolioSlider.textOpaity = 1;
            } else {
                return;
            }
        }

        portfolioSlider.slideWidth = portfolioSlider.slideWidth - ((portfolioSlider.slideMaxWidth - portfolioSlider.slideDefaultWidth) / portfolioSlider.speed);
        $('.js-portfolio-card.active').css('width', portfolioSlider.slideWidth);
        console.log(portfolioSlider.slideWidth, portfolioSlider.slideDefaultWidth + (portfolioSlider.slideMaxWidth - portfolioSlider.slideDefaultWidth) / portfolioSlider.speed * 2);
        if (portfolioSlider.textMargin < 0 && portfolioSlider.slideWidth >= portfolioSlider.slideDefaultWidth + (portfolioSlider.slideMaxWidth - portfolioSlider.slideDefaultWidth) / portfolioSlider.speed * 2) {
            portfolioSlider.textMargin = portfolioSlider.textMargin + 70 / 2;
            portfolioSlider.textOpaity = portfolioSlider.textOpaity - 1 / 2;
        }
        $('.js-portfolio-card.active .mesh-section_first__portfolio-card-text').css('margin-top', portfolioSlider.textMargin);
        $('.js-portfolio-card.active .mesh-section_first__portfolio-card-text').css('opacity', portfolioSlider.textOpaity);
    }

    function scrollRight() {
        if (portfolioSlider.slideWidth == portfolioSlider.slideMaxWidth) {
            const nextSlide = $('.js-portfolio-card.active').next();
            //Проверка на то, есть ли дальше ещё слайды
            if (nextSlide.hasClass('js-portfolio-card')) {
                portfolioSlider.slideWidth = portfolioSlider.slideDefaultWidth;
                opacity = 0.1;
                $('.js-portfolio-card.active').removeClass('active');
                nextSlide.addClass('active');
                portfolioSlider.textMargin = 0;
                portfolioSlider.textOpaity = 0;
            } else {
                return;
            }
        }

        portfolioSlider.slideWidth = portfolioSlider.slideWidth + ((portfolioSlider.slideMaxWidth - portfolioSlider.slideDefaultWidth) / portfolioSlider.speed);
        $('.js-portfolio-card.active').css('width', portfolioSlider.slideWidth);

        if (portfolioSlider.textMargin > -70 && portfolioSlider.slideWidth > portfolioSlider.slideDefaultWidth + (portfolioSlider.slideMaxWidth - portfolioSlider.slideDefaultWidth) / portfolioSlider.speed * 2) {
            portfolioSlider.textMargin = portfolioSlider.textMargin - 70 / 2;
            portfolioSlider.textOpaity = portfolioSlider.textOpaity + 1 / 2;
        }
        $('.js-portfolio-card.active .mesh-section_first__portfolio-card-text').css('margin-top', portfolioSlider.textMargin);

        $('.js-portfolio-card.active .mesh-section_first__portfolio-card-text').css('opacity', portfolioSlider.textOpaity);
    }


    $('.mesh-section_first__portfolio').on('wheel', function (e) {
        e.preventDefault();
        // Задаём направление скролла
        if (e.originalEvent.deltaY < 0) {
            portfolioSlider.direction = 'left';
        } else {
            portfolioSlider.direction = 'right';
        }


        // Определяем вызываемую функцию в зависимости от напрваления
        if (portfolioSlider.direction == 'right') {

            scrollRight();
        } else {

            scrollLeft();
        }

        //Проверка на левую границу
        if (portfolioSlider.offset !== 0 || portfolioSlider.direction !== 'left') {   //Проверка на правую границу
            if (portfolioSlider.offset < portfolioSlider.slideMaxWidth * (portfolioSlider.slidesCount - 1) || portfolioSlider.direction !== 'right') {
                //Не перемещаем контейнер, пока последний слайд не уменьшится
                if (!$('.js-portfolio-card:last-of-type').hasClass('active')) {
                    if (portfolioSlider.direction == 'right') {
                        portfolioSlider.offset = portfolioSlider.offset + (portfolioSlider.slideMaxWidth / portfolioSlider.speed);
                    } else {
                        portfolioSlider.offset = portfolioSlider.offset - (portfolioSlider.slideMaxWidth / portfolioSlider.speed);
                    }
                }
            }

        }
        // Перемещение контейнера
        $('.mesh-section_first .mesh-section__content').css('margin-left', -portfolioSlider.offset);

    });
    /********** Слайдер портфолио (конец)**********/

    /********** Выбор вкладки в секции отраслевые решения для бизнеса **********/
    $('.js-mesh-decisions-section-nav-item').click(function () {
        $('.js-mesh-decisions-section-nav-item').removeClass('active');
        $(this).addClass('active');
    });
    /********** Выбор вкладки в секции отраслевые решения для бизнеса (конец) **********/

    /********** Слайдер статистики **********/
    let statisticSlider = new Swiper('.swiper-container.js-statistic-slider', {
        slidesPerView: 1,
        pagination: {
            el: ".js-statistic-slider-pagination",
        },
    });
    /********** Слайдер статистики (конец) **********/

    /********** Слайдер лицензии **********/
    let licenseSlider = new Swiper('.swiper-container.js-license-slider', {
        slidesPerView: 'auto',
        spaceBetween: 0,
        freeMode: true,
        navigation: {
            nextEl: ".js-license-slider-nav-next",
            prevEl: ".js-license-slider-nav-prev",
        },
    });
    /********** Слайдер лицензии (конец) **********/

    /********** Переключение фильтров слайдера в блоке Лицензии **********/
    $('.js-mesh-license-section-nav-item').click(function () {
        $('.js-mesh-license-section-nav-item').removeClass('active');
        $(this).addClass('active');
    });
    /********** Переключение фильтров слайдера в блоке Лицензии (конец) **********/

    /********** Инициализация плитики **********/
    $('.js-mesh-outsource-section-cards-container').isotope({
        // options
        itemSelector: '.js-outsource-item',
        layoutMode: 'packery',
        packery: {
            gutter: '.outsource-gutter',
            horizontal: true
        }
    });

    $('.js-mesh-outsource-section-cards-container-main').isotope({
        // options
        itemSelector: '.js-outsource-item-main',
        layoutMode: 'packery',
        packery: {
            gutter: '.outsource-gutter-main',
            columnWidth: '.outsource-section .mesh-outsource-section__card'
        },
        percentPosition: true,
    });
    /********** Инициализация плитики (конец) **********/

    /**** Перестраивание после загрузки картинок ****/
    $('.js-mesh-outsource-section-cards-container-main').imagesLoaded().progress(function () {
        $('.js-mesh-outsource-section-cards-container-main').isotope('layout');
    });

    $('.js-mesh-outsource-section-cards-container').imagesLoaded().progress(function () {
        $('.js-mesh-outsource-section-cards-container').isotope('layout');
    });
    /**** Перестраивание после загрузки картинок (конец) ****/

    /********** Аутсорс сортировка **********/
    $('.js-mesh-outsource-section-nav-item').click(function () {
        const categoryName = $(this).data('cat');
        $('.js-mesh-outsource-section-nav-item').removeClass('active');
        $(this).addClass('active');
        ;
        if (categoryName === 'all') {
            $('.js-mesh-outsource-section-cards-container-main').isotope({filter: '*'});
        } else {
            $('.js-mesh-outsource-section-cards-container-main').isotope({filter: '.' + categoryName});
        }
    });

    /********** Аутсорс сортировка **********/

    /********** Анимация появление заголовков **********/
    function titleShow(obj, duration) {
        let sumboldCount = $(obj).children('span').length - 1;
        let x = 0;

        let mainInterval = setInterval(function () {
            $(obj + ' span:eq(' + x + ')').animate({'opacity': 1, 'bottom': 0}, duration, function () {
                if (x > $(obj).children('span').length - 2) {
                    $(obj).parent('.js-mesh-section__title').children('span').animate({'opacity': '1'}, duration);
                }
            });
            x++
            if (x > $(obj).children('span').length - 1) {
                clearInterval(mainInterval);
            }
        }, 100);
    }

    function getProgess(obj, duration) {
        $(obj).animate({'width': '95%'}, duration);
    }

    function showImageSort(obj, duration) {
        $(obj).animate({'opacity': '1'}, duration);
    }

    function showImage(obj, duration) {
        $(obj).animate({'opacity': '1', 'bottom': 0}, duration);
    }

    $(window).scroll(function () {

        $('.js-title-animation-block').each(function (key) {
            var x = 0;
            if ($(window).scrollTop() > $(this).offset().top - $(window).height() + 200 && x == 0) {
                titleShow('.js-title-animation-block:eq(' + key + ')', 400);
                x = 1;
            }
        });

        $('.js-mesh-outstaff-section__stat-line-progress-fill').each(function (key) {
            var x = 0;
            if ($(window).scrollTop() > $(this).offset().top - $(window).height() && x == 0) {
                getProgess('.js-mesh-outstaff-section__stat-line-progress-fill:eq(' + key + ')', 700);
                x = 1;
            }
        });
//todo shalser-----------------------------------------------------------------------------
        $('.js-mesh-vacancies-section__stat-line-progress-fill').each(function (key) {
            var x = 0;
            if ($(window).scrollTop() > $(this).offset().top - $(window).height() && x == 0) {
                getProgess('.js-mesh-vacancies-section__stat-line-progress-fill:eq(' + key + ')', 700);
                x = 1;
            }
        });
//todo shalser-----------------------------------------------------------------------------
    });


    /********** Анимация появление заголовков (конец) **********/
    /********** Слайдер аутстаф **********/
    let outstaffSlider = new Swiper('.swiper-container.event-content', {
        slidesPerView: 1,
    });
//todo shalser-----------------------------------------------------------------------------
    // let vacanciesSlider = new Swiper('.swiper-container.event-content', {
    //     slidesPerView: 1,
    // });
    //todo shalser-----------------------------------------------------------------------------
    /********** Слайдер аутстаф (конец) **********/


    /********** Слайдер отзывы **********/
    if ($(window).width() > 760) {
        let reviewSlider = new Swiper('.swiper-container.review-slider', {
            slidesPerView: 2,
            spaceBetween: 30,
            observer: true,
            observeParents: true,
            navigation: {
                nextEl: ".js-review-slider-next",
                prevEl: ".js-review-slider-prev",
            },
        });
    } else {
        let reviewSlider = new Swiper('.swiper-container.review-slider', {
            slidesPerView: 1,
            spaceBetween: 30,
            observer: true,
            observeParents: true,
            navigation: {
                nextEl: ".js-review-slider-next",
                prevEl: ".js-review-slider-prev",
            },
        });
    }

    /********** Слайдер отзывы (конец) **********/

    function portfolio_slider_content($this, $portfolio_content) {
        var $el = jQuery($this.slides[$this.activeIndex]),
            title = $el.attr('data-title'),
            desc = $el.attr('data-desc'),
            link = $el.attr('data-link');

        if (title) {
            $portfolio_content.find('.title').text(title).fadeIn();
        }
        if (desc) {
            $portfolio_content.find('.desc').text(desc).fadeIn();
        }
        if (link) {
            $portfolio_content.find('a').attr('href', link).fadeIn();
        }
    }

    jQuery(document).ready(function (jQuery) {
        var $portfolio_slider = jQuery('.portfolio-5e79b51c2bcba'),
            $portfolio_content = $portfolio_slider.find('.content'),
            $portfolio_slider_swiper = new Swiper($portfolio_slider.find('.swiper-container'), {
                loop: true,
                slidesPerView: 'auto',
                watchSlidesVisibility: true,
                spaceBetween: 30,
                loopAdditionalSlides: 4,
                navigation: {
                    nextEl: $portfolio_slider.find('.prev'),
                },
                on: {
                    setTranslate: function (e) {
                        var $this = this;

                        jQuery(this.slides).each(function (index) {
                            if (jQuery('html').attr('dir') === 'rtl') {
                                var left = $this.slidesGrid[index],
                                    x = $this.translate - left,
                                    k = 1 - x / jQuery(this).outerWidth(true),
                                    o = k < 0 ? 0 : k * 0.5 + 0.5,
                                    scale = (k * 0.2 + 0.8);

                                x = -$this.translate + left;
                                x = x >= 0 ? 0 : x;

                                scale = scale > 1 ? 1 : scale;
                                scale = scale <= 0 ? 0 : scale;
                            } else {
                                var left = $this.slidesGrid[index],
                                    x = -$this.translate - left,
                                    k = 1 - x / jQuery(this).outerWidth(true),
                                    o = k < 0 ? 0 : k * 0.5 + 0.5,
                                    scale = (k * 0.2 + 0.8);

                                x = x <= 0 ? 0 : x;

                                scale = scale > 1 ? 1 : scale;
                                scale = scale <= 0 ? 0 : scale;
                            }

                            jQuery(this).css({
                                'transform': 'translateX(' + (x) + 'px) scale(' + (scale) + ')',
                                opacity: o,
                                transition: jQuery($this.$wrapperEl).css('transition-duration')
                            })
                        })
                    },
                    transitionStart: function () {
                        $portfolio_content.find('> *').stop().fadeOut();
                    },
                    transitionEnd: function () {
                        portfolio_slider_content(this, $portfolio_content);
                    },
                    init: function () {
                        portfolio_slider_content(this, $portfolio_content);
                    }
                }
            });
    });


    jQuery('body').on('click', '.js-mesh-review-section-nav-item', function () {
        $('.js-mesh-review-section-nav-item').removeClass('active');
        $(this).addClass('active');
        let $block = $('.js-preview-portfolio-type-slider'),
            filter_val = jQuery(this).attr('data-filter'),
            $swiper_container = $block.find('.swiper-container'),
            swiper = $swiper_container.get(0).swiper;

        jQuery(this).addClass('current').siblings().removeClass('current');

        if (filter_val != '*') {
            $swiper_container.addClass('loading').delay(300).queue(function (next) {
                jQuery(this).find('.swiper-slide').not(filter_val).each(function () {
                    jQuery(this).addClass('non-swiper-slide').removeClass('swiper-slide').hide();
                });

                next();
            });
            $swiper_container.find(filter_val).delay(300).queue(function (next) {
                jQuery(this).each(function () {
                    jQuery(this).removeClass('non-swiper-slide').addClass('swiper-slide').attr('style', null).show();
                });

                next();
            });
        } else {
            $swiper_container.find('.non-swiper-slide').delay(300).queue(function (next) {
                jQuery(this).each(function () {
                    jQuery(this).removeClass('non-swiper-slide').addClass('swiper-slide').attr('style', null).show();
                });

                next();
            });
        }

        setTimeout(function () {
            swiper.update();
            swiper.slideTo(0, 0);
            jQuery(window).trigger('resize');

            $swiper_container.removeClass('loading');
        }, 300)

    });

    $('.js-select-outstaff-slide').click(function () {
        $('.js-select-outstaff-slide').removeClass('active');
        $(this).addClass('active');
        outstaffSlider.slideTo($(this).index());
    });
    outstaffSlider.on('slideChange', function () {
        let sIndex = outstaffSlider.activeIndex + 1;
        $('.js-select-outstaff-slide').removeClass('active');
        $('.js-select-outstaff-slide:nth-of-type(' + sIndex + ')').addClass('active');
    });


    jQuery(document).ready(function (jQuery) {
        var $gallery_carousel = jQuery('.photo-carousel-block-5e8ef64909ad2'),
            $gallery_carousel_swiper = new Swiper($gallery_carousel.find('.swiper-container'), {
                loop: true,

                navigation: {
                    nextEl: $gallery_carousel.find('.next'),
                    prevEl: $gallery_carousel.find('.prev'),
                },
                slidesPerView: 'auto',
                loopAdditionalSlides: 4,
            });
    });

    /************** Изменение цвета фона **********/
    if ($(window).width() > 1200) {
        $(window).on('scroll', function () {
            if ($('.outsource-section').length > 0) {
                if ($(window).scrollTop() > $('.outsource-section').offset().top - 500) {

                    $('body').css('background-color', '#282828');
                    $('.mesh-section__header-title h2').css('color', 'white');
                    $('.mesh-section__header-back-text').css('color', 'rgb(255,255,255,0.05)');
                    $('.mesh-section__header-title span').css('color', 'white');
                    $('.mesh-section__header-text').css('color', 'white');
                    $('.mesh-content-section__nav ul li').css('color', 'white');
                    $('.license-section__see-more-href>div').css('background-color', 'white');
                    $('.license-section__see-more-href>a').css('color', 'white');
                } else {
                    $('body').css('background-color', 'white');
                    $('.mesh-section__header-title h2').css('color', 'black');
                    $('.mesh-section__header-back-text').css('color', '#EEEEEE');
                    $('.mesh-section__header-title span').css('color', '#535353');
                    $('.mesh-section__header-text').css('color', 'black');
                    $('.mesh-content-section__nav ul li').css('color', 'black');
                    $('.license-section__see-more-href>div').css('background-color', 'black');
                    $('.license-section__see-more-href>a').css('color', 'black');
                }
                if ($(window).scrollTop() > $('.outsource-section').offset().top + 500) {

                    $('body').css('background-color', 'white');
                    $('.mesh-section__header-title h2').css('color', 'black');
                    $('.mesh-section__header-back-text').css('color', '#EEEEEE');
                    $('.mesh-section__header-title span').css('color', '#535353');
                    $('.mesh-section__header-text').css('color', 'black');
                    $('.mesh-content-section__nav ul li').css('color', 'black');
                    $('.license-section__see-more-href>div').css('background-color', 'black');
                    $('.license-section__see-more-href>a').css('color', 'black');
                }
            }


        });
    }
    /********** Навигация слева **********/
    var x = 0;
    var lastScrollTopNav = 0;
    $(window).on('scroll', function () {

        var st = $(this).scrollTop();
        if (st > lastScrollTopNav) {
            if ($('section:eq(' + x + ')').length > 0) {
                if ($(window).scrollTop() > $('section:eq(' + x + ')').offset().top) {
                    const id = $('section:eq(' + x + ')').attr('id');
                    $('.site-navigation-left li').removeClass('active');
                    $('.site-navigation-left li[data-section=' + id + ']').addClass('active');
                    x++;
                }
            }

        } else {
            if ($('section:eq(' + x + ')').length > 0) {
                if ($(window).scrollTop() < $('section:eq(' + (x - 1) + ')').offset().top + $('section:eq(' + (x - 1) + ')').height() && x > 0) {
                    const id = $('section:eq(' + (x - 1) + ')').attr('id');
                    $('.site-navigation-left li').removeClass('active');
                    $('.site-navigation-left li[data-section=' + id + ']').addClass('active');
                    x--;
                }
            }

        }
        lastScrollTopNav = st;

    });


    /********** Навигация слева (конец) **********/
    /********** Инициализация плитики **********/
    $('.js-mesh-news-section-cards-container').isotope({
        // options
        itemSelector: '.js-news-item',
        layoutMode: 'packery',
        packery: {
            gutter: '.news-gutter'
        },
        percentPosition: true
    });
    /********** Инициализация плитики (конец) **********/

    /**** Перестраивание после загрузки картинок ****/
    $('.js-mesh-news-section-cards-container').imagesLoaded().progress(function () {
        $('.js-mesh-news-section-cards-container').isotope('layout');
    });
    /**** Перестраивание после загрузки картинок (конец) ****/

    /********** Аутсорс сортировка **********/
    $('.js-mesh-news-section-nav-item').click(function () {
        const categoryName = $(this).data('cat');
        $('.js-mesh-news-section-nav-item').removeClass('active');
        $(this).addClass('active');
        ;
        if (categoryName === 'all') {
            $('.js-mesh-news-section-cards-container').isotope({filter: '*'});
        } else {
            $('.js-mesh-news-section-cards-container').isotope({filter: '.' + categoryName});
        }
    });
    /********** Аутсорс сортировка **********/


    if ($(window).width() < 1200) {

        if ($(window).width() < 760) {
            var clientsMobileSliderSlidesPerView = 3;
            var buisnessMobileSliderSlidesPreView = 2;
        } else {
            var clientsMobileSliderSlidesPerView = 4;
            var buisnessMobileSliderSlidesPreView = 3;
        }
        /********** Слайдер навигации ***********/
        let eventNavSlider = new Swiper('.swiper-container.event-nav-swiper', {
            slidesPerView: 'auto',
            observer: true,
            observeParents: true,
            spaceBetween: 0,
            // navigation: {
            //     nextEl: ".js-clients-slider-right-arrow",
            //     prevEl: ".js-clients-slider-left-arrow",
            //   },
        });

        /********** Слайдер клиентов на мобилке **********/
        let clientsMobileSlider = new Swiper('.swiper-container.js-clients-slider', {
            slidesPerView: clientsMobileSliderSlidesPerView,
            observer: true,
            autoplay: {
                delay: 3000,
            },
            observeParents: true,
            spaceBetween: 0,
            navigation: {
                nextEl: ".js-clients-slider-right-arrow",
                prevEl: ".js-clients-slider-left-arrow",
            },
        });
        /********** Слайдер клиентов на мобилке (конец) **********/

        /********** Слайдер бизнес на мобилке **********/
        let buisnessMobileSlider = new Swiper('.swiper-container.buisness-slider', {
            slidesPerView: buisnessMobileSliderSlidesPreView,
            slidesPerColumn: 2,
            autoplay: {
                delay: 3000,
            },
            spaceBetween: 0,
            slidesPerGroup: 2,
            slidesPerColumnFill: 'row',
            pagination: {
                el: '.js-buisness-slider-navigation',
                clickable: true,
            },
        });
        /********** Слайдер бизнес на мобилке (конец) **********/

    }


    if ($(window).width() > 1200) {

        /********** Анимации на страницу лука **********/

        let margin = 0;
        let lastScrollTop = 0;
        let currentSlide = 0;
        let newSlide = 0;
        let summHeight = 0;
// let scrollForHeader = 0;
// let scrollHeaderDirectionFlaf = false;
// let savedScrollTopForHeader = 0;
// let scrollForSticky = 0;
        $(document).scroll(function (e) {

            /********** Появление стреки скролла вверх *********/
            if ($(this).scrollTop() > 700) {
                $('.single-look__scroll-up').css('display', 'flex');
            } else {
                $('.single-look__scroll-up').css('display', 'none');
            }
            /********** Появление стреки скролла вверх (конец) *********/

            let documentHeight = 0;
            let headeHeight = $('.mesh-header').height() + parseInt($('.mesh-header').css('padding-top')) + parseInt($('.mesh-header').css('padding-bottom'));
            $('.single-look-main-container article').each(function () {
                documentHeight = documentHeight + $(this).height() + parseInt($(this).css('padding-top')) + parseInt($(this).css('padding-bottom'));
            });
            documentHeight = headeHeight + documentHeight;
            console.log(documentHeight);
            $('.single-look-page').css('height', documentHeight);
            let st = $(this).scrollTop();
            let thisSlideHeight = $('.active-article-slide').height() + parseInt($('.active-article-slide').css('padding-top')) + parseInt($('.active-article-slide').css('padding-bottom'));

            $('.single-look-header').css('margin-top', -st);
            if (st > lastScrollTop) {

                if (-parseInt($('.active-article-slide').css('margin-top')) > thisSlideHeight - $(window).height()) {
                    let swipedSlidesHeight = 0;


                    for (let x = 0; x < currentSlide + 1; x++) {
                        swipedSlidesHeight = swipedSlidesHeight + $('.single-look-main-container article:eq(' + x + ')').height() + parseInt($('.single-look-main-container article:eq(' + x + ')').css('padding-top')) + parseInt($('.single-look-main-container article:eq(' + x + ')').css('padding-bottom'));
                    }
                    let brightnessPercent = 100 / $(window).height() * (swipedSlidesHeight - st);
                    brightnessPercent = Math.round(brightnessPercent);
                    brightnessPercent = 100 - brightnessPercent;
                    // brightnessPercent = parseInt(brightnessPercent/10);

                    if (brightnessPercent > 99) {
                        brightnessPercent = 1;
                    } else {
                        if (brightnessPercent < 10) {
                            brightnessPercent = '0' + brightnessPercent;
                        }
                        brightnessPercent = parseFloat('0.' + brightnessPercent);
                    }

                    $('.active-article-next-slide').css('filter', 'brightness(' + brightnessPercent + ')');
                }


                if (-parseInt($('.active-article-slide').css('margin-top')) > thisSlideHeight) {
                    summHeight = summHeight + thisSlideHeight;
                    $('.active-article-slide').next().addClass('active-article-slide');
                    $('.main-current-slide').removeClass('active-article-slide');
                    $('.main-current-slide').removeClass('main-current-slide');
                    $('.active-article-slide').removeClass('active-article-next-slide');
                    $('.active-article-slide').addClass('main-current-slide');
                    $('.active-article-slide').next().addClass('active-article-next-slide');
                    currentSlide++;
                }

            } else {
                if (-parseInt($('.active-article-slide').css('margin-top')) > thisSlideHeight - $(window).height()) {
                    let swipedSlidesHeight = 0;


                    for (let x = 0; x < currentSlide + 1; x++) {
                        swipedSlidesHeight = swipedSlidesHeight + $('.single-look-main-container article:eq(' + x + ')').height() + parseInt($('.single-look-main-container article:eq(' + x + ')').css('padding-top')) + parseInt($('.single-look-main-container article:eq(' + x + ')').css('padding-bottom'));
                    }
                    let brightnessPercent = 100 / $(window).height() * (swipedSlidesHeight - st);
                    brightnessPercent = Math.round(brightnessPercent);
                    brightnessPercent = 100 - brightnessPercent;
                    // brightnessPercent = parseInt(brightnessPercent/10);
                    if (brightnessPercent < 1) {
                        brightnessPercent = 0;
                    } else {
                        if (brightnessPercent < 10) {
                            brightnessPercent = '0' + brightnessPercent;
                        }
                        brightnessPercent = parseFloat('0.' + brightnessPercent);
                    }
                    $('.active-article-next-slide').css('filter', 'brightness(' + brightnessPercent + ')');
                }

                if (parseInt($('.active-article-slide').css('margin-top')) > -1) {
                    $('.active-article-slide').addClass('active-article-next-slide');
                    $('.active-article-slide').next().removeClass('active-article-next-slide');
                    $('.active-article-slide').prev().addClass('active-article-slide');
                    $('.main-current-slide').removeClass('active-article-slide');
                    $('.main-current-slide').removeClass('main-current-slide');
                    $('.active-article-slide').addClass('main-current-slide');
                    currentSlide--;
                    thisSlideHeight = $('.active-article-slide').height() + parseInt($('.active-article-slide').css('padding-top')) + parseInt($('.active-article-slide').css('padding-bottom'));
                    summHeight = summHeight - thisSlideHeight;
                }

            }
            margin = -st + summHeight;
            lastScrollTop = st;
            if (margin > 0) {
                margin = 0;
            }
            $('.single-look-page article:eq(' + currentSlide + ')').css('margin-top', margin);

        });
        /********** Анимации на страницу лука (конец) **********/
    }
    /********** Скролл на верх страницы **********/
    $('body').on('click', '.js-scroll-to-top', function () {
        $('html,body').stop().animate({scrollTop: 0}, 1000);
    });
    /********** Скролл на верх страницы (конец) **********/
    /********** Инициализация слайдера тэгов **********/
    let tagsSwiper = new Swiper('.swiper-container.js-single-look-tags-slider', {
        slidesPerView: 'auto',
        spaceBetween: 10,

    });
    /********** Инициализация слайдера тэгов (конец) **********/

    /********** Инициализация слайдера картинок на странице лука **********/
    let singleLookImageSwiper = new Swiper('.swiper-container.js-single-look-images-slider', {
        spaceBetween: 30,
        effect: 'fade',
        pagination: {
            el: '.js-single-look__images-slider-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.js-single-look__images-slider-next-arrow',
            prevEl: '.js-single-look__images-slider-prev-arrow',
        },
    });
    /********** Инициализация слайдера картинок на странице лука (конец) **********/


    /********** Инициализация слайдера картинок на странице лука **********/
    let charityMainSwiper = new Swiper('.swiper-container.js-charity-section-slider', {
        spaceBetween: 0,
        effect: 'fade',
        pagination: {
            el: '.js-charity-section-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.js-charity-section-slider-next',
            prevEl: '.js-charity-section-slider-prev',
        },
    });
    /********** Инициализация слайдера картинок на странице лука (конец) **********/

    /*************** Личная страница портфолио ***************/

    /********** scroll slider **********/
    var scrollSwiper = new Swiper(".js-single-portfolio-horizontal-slider-section-slider", {
        freeMode: true,
        slidesPerView: 'auto',
        scrollbar: {
            el: ".swiper-scrollbar",
            hide: true,
        },
    });
    /********** scroll slider **********/
    var scrollSwiperTwo = new Swiper(".js-single-portfolio-horizontal-slider-section-slider-two", {
        spaceBetween: 30,
        slidesPerView: 'auto',
        pagination: {
            el: '.single-portfolio-horizontal-slider-section__pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.single-portfolio-horizontal-slider-section__right-arrow',
            prevEl: '.single-portfolio-horizontal-slider-section__lefr-arrow',
        },

    });

    /*************** Личная страница портфолио (конец) ***************/
    if ($('.js-single-portfolio-horizontal-slider-section-slider-three .swiper-slide').length > 3) {
        var scrollSwiperThree = new Swiper(".js-single-portfolio-horizontal-slider-section-slider-three", {
            spaceBetween: 30,
            slidesPerView: 'auto',
            loop: 'true',
            navigation: {
                nextEl: '#numeric-slider-section .single-portfolio-horizontal-slider-section__right-arrow',
                prevEl: '#numeric-slider-section .single-portfolio-horizontal-slider-section__lefr-arrow',
            },

        });
    } else {
        $('#numeric-slider-section .single-portfolio-horizontal-slider-section__navigarion').hide();
    }
    /************ Главный слайдер на мобильной версии страницы ***********/

    if ($(window).width() < 760) {
        var mobileMainSwiperSlidesPreView = 2;
    } else {
        var mobileMainSwiperSlidesPreView = 3;
    }

    let mobileMainSwiper = new Swiper('.swiper-container.mobile-main-slider', {
        spaceBetween: 10,
        slidesPerView: mobileMainSwiperSlidesPreView,
        pagination: {
            el: '.mobile-main-slider-pagination',
            clickable: true,
        },
    });

    let lorealProjectSlider = new Swiper('.js-key-visual-images-swiper', {
        spaceBetween: 20,
        slidesPerView: 3,
    });

    function SelectActiveMenuItem(target, menuItemClass, activeClass) {
        $(menuItemClass).removeClass(activeClass);
        $(target).addClass(activeClass);
    }

    $('.js-mesh-section_first-portfolio-nav-item').click(function () {
        SelectActiveMenuItem($(this), '.js-mesh-section_first-portfolio-nav-item', 'active');
        const cat = $(this).data('cat');
        $('.js-mobile-main-slider').animate({'opacity': 0}, 300, function () {
            if (cat == 'all') {
                $('.js-mobile-main-slider .swiper-slide').css('display', 'flex');
            } else {
                $('.js-mobile-main-slider .swiper-slide').each(function () {
                    const slideCat = $(this).data('cat');
                    if (!$(this).hasClass(cat)) {
                        $(this).css('display', 'none');
                    } else {
                        $(this).css('display', 'flex');
                    }
                });
            }
            mobileMainSwiper.update();
            $('.js-mobile-main-slider').animate({'opacity': 1}, 300);
        });
    });


    jQuery(document).ready(function (jQuery) {
        var $portfolio_carousel = jQuery('.portfolio-5e9d48f197c46'),
            $portfolio_carousel_swiper = new Swiper($portfolio_carousel.find('.swiper-container'), {
                spaceBetween: 30,
                navigation: {
                    nextEl: $portfolio_carousel.find('.prev'),
                    prevEl: $portfolio_carousel.find('.next'),
                },
                breakpoints: {
                    200: {slidesPerView: 'auto'}, 576: {slidesPerView: 2}, 768: {slidesPerView: 3},
                }
            });
    });

    /********** Изменение цвеета фона лоераль *********/
    if ($(window).width() > 1200) {
        $(window).on('scroll', function () {
            if ($('#single-portfolio-grid-section').length > 0) {
                if ($(window).scrollTop() > $('#single-portfolio-grid-section').offset().top - 500) {

                    $('body').addClass('dark-theme');
                    $('.mesh-section__header-title h2').addClass('dark-theme');
                    $('.single-portfolio-header__back-text').addClass('dark-theme');
                    $('.single-portfolio-header__right-part p').addClass('dark-theme');
                    $('.mesh-section__header-title span').addClass('dark-theme');
                    $('.mesh-section__header-text').css('color', 'white');
                    $('.mesh-content-section__nav ul li').css('color', 'white');
                    $('.license-section__see-more-href>div').css('background-color', 'white');
                    $('.license-section__see-more-href>a').css('color', 'white');
                } else {
                    $('body').removeClass('dark-theme');
                    $('.mesh-section__header-title h2').removeClass('dark-theme');
                    $('.single-portfolio-header__back-text').removeClass('dark-theme');
                    $('.single-portfolio-header__right-part p').removeClass('dark-theme');
                    $('.mesh-section__header-title span').removeClass('dark-theme');
                    $('.mesh-section__header-text').css('color', 'black');
                    $('.mesh-content-section__nav ul li').css('color', 'black');
                    $('.license-section__see-more-href>div').css('background-color', 'black');
                    $('.license-section__see-more-href>a').css('color', 'black');
                }
                if ($(window).scrollTop() > $('#single-portfolio-grid-section').offset().top + 500) {

                    $('body').removeClass('dark-theme');
                    $('.mesh-section__header-title h2').removeClass('dark-theme');
                    $('.single-portfolio-header__back-text').removeClass('dark-theme');
                    $('.single-portfolio-header__right-part p').removeClass('dark-theme');
                    $('.mesh-section__header-title span').removeClass('dark-theme');
                    $('.mesh-section__header-text').css('color', 'black');
                    $('.mesh-content-section__nav ul li').css('color', 'black');
                    $('.license-section__see-more-href>div').css('background-color', 'black');
                    $('.license-section__see-more-href>a').css('color', 'black');
                }
            }


        });
    }

    /************* Открытие и закрытие пунктов меню **************/

    function showAndHideMobileMenuItems(menu, item) {

        if (isMobile) {
            $('.mesh-header .mesh-nav>ul li').removeClass('active-mobile');
            item.addClass('active-mobile');
            $('.header-menu').removeClass('active');
            $('.header-menu').stop().fadeOut(700);
            menu.addClass('active');
            menu.stop().fadeIn(700);
        }

    }

    function showDesctopMenuItems(menu, item) {

        if (!isMobile) {
            menu.css('z-index', 2);
            item.addClass('active');
            menu.addClass('active');
            menu.stop().fadeTo(500, 1);
        }

    }

    function hideDesctopMenuItems(menu, item) {

        if (!isMobile) {

            menu.css('z-index', 1);
            menu.stop().fadeTo(500, 0, function () {
                menu.css('display', 'none');
            });
            menu.removeClass('active');
            item.removeClass('active');
            // $('.mesh-header .mesh-nav>ul li').removeClass('active');
            // $('.header-menu').removeClass('active');
            // $('.header-menu').stop().fadeOut(700);
        }

    }

    $('body').on('mouseenter', '.js-open-menu-license,.js-header-menu-license', function () {
        const thisEl = $(this);
        showDesctopMenuItems($('.js-header-menu-license'), thisEl);
    });

    $('body').on('mouseenter', '.js-menu-outsourcing,.js-header-menu-outsourcing', function () {
        const thisEl = $(this);
        showDesctopMenuItems($('.js-header-menu-outsourcing'), thisEl);
    });

    $('body').on('mouseenter', '.js-menu-outstaff,.js-header-menu-outstaff', function () {
        const thisEl = $(this);
        showDesctopMenuItems($('.js-header-menu-outstaff'), thisEl);
    });

    //todo shalser start -----------------------------------------------------------------------------

    $('body').on('mouseenter', '.js-menu-vacancies,.js-header-menu-vacancies', function () {
        const thisEl = $(this);
        showDesctopMenuItems($('.js-header-menu-vacancies'), thisEl);
    });

    //todo shalser end-----------------------------------------------------------------------------

    $('body').on('mouseleave', '.js-open-menu-license,.js-header-menu-license', function () {
        const thisEl = $(this);
        hideDesctopMenuItems($('.js-header-menu-license'), thisEl);
    });

    $('body').on('mouseleave', '.js-menu-outsourcing,.js-header-menu-outsourcing', function () {
        const thisEl = $(this);
        hideDesctopMenuItems($('.js-header-menu-outsourcing'), thisEl);
    });

    $('body').on('mouseleave', '.js-menu-outstaff,.js-header-menu-outstaff', function () {
        const thisEl = $(this);
        hideDesctopMenuItems($('.js-header-menu-outstaff'), thisEl);
    });

    //todo shalser start -----------------------------------------------------------------------------

    $('body').on('mouseleave', '.js-menu-vacancies,.js-header-menu-vacancies', function () {
        const thisEl = $(this);
        hideDesctopMenuItems($('.js-header-menu-vacancies'), thisEl);
    });

    //todo shalser end----------------------------------------------------------------------------

    $('body').on('click', '.js-open-menu-license', function () {
        const thisEl = $(this);
        showAndHideMobileMenuItems($('.js-header-menu-license'), thisEl);
    });

    $('body').on('click', '.js-menu-outsourcing', function () {
        const thisEl = $(this);
        showAndHideMobileMenuItems($('.js-header-menu-outsourcing'), thisEl);
    });

    $('body').on('click', '.js-menu-outstaff', function () {
        const thisEl = $(this);
        showAndHideMobileMenuItems($('.js-header-menu-outstaff'), thisEl);
    });


    //todo shalser start -----------------------------------------------------------------------------

    $('body').on('click', '.js-menu-vacancies', function () {
        const thisEl = $(this);
        showAndHideMobileMenuItems($('.js-header-menu-vacancies'), thisEl);
    });

    //todo shalser end-----------------------------------------------------------------------------

    /*********** Слайдер в выпадающем меню аутстаф (хедер) **********/
    $('.js-header-menu-outstaff-section').each(function (item, index) {
        const section = $(this).data('section');
        if (isMobile) {
            let headerOutstaffSlider = [];
            headerOutstaffSlider[section] = new Swiper('.js-header-menu-stack-slider-' + section, {
                spaceBetween: 16,
                slidesPerView: 'auto',
                freeMode: true,
                navigation: {
                    nextEl: '.js-header-menu-stack-slider-prev-arrow-' + section,
                    prevEl: '.js-header-menu-stack-slider-next-arrow-' + section
                },
                observeParents: true,
                observer: true
            });

        } else {
            const slides = $(this).find('.swiper-container').data('slides');
            let headerOutstaffSlider = [];
            headerOutstaffSlider[section] = new Swiper('.js-header-menu-stack-slider-' + section, {
                spaceBetween: 35,
                slidesPerView: slides,
                navigation: {
                    nextEl: '.js-header-menu-stack-slider-prev-arrow-' + section,
                    prevEl: '.js-header-menu-stack-slider-next-arrow-' + section
                },
                observeParents: true,
                observer: true
            });
        }


    });

    //todo shalser start -----------------------------------------------------------------------------

    /*********** Слайдер в выпадающем меню vacancies (хедер) **********/

    $('.js-header-menu-vacancies-section').each(function(item,index){
        const section = $(this).data('section');
        if(isMobile) {
            let headerVacanciesSlider = [];
            headerVacanciesSlider[section] = new Swiper('.js-header-menu-stack-slider-'+section, {
                spaceBetween: 16,
                slidesPerView:'auto',
                freeMode: true,
                navigation: {
                    nextEl:'.js-header-menu-stack-slider-prev-arrow-'+section,
                    prevEl:'.js-header-menu-stack-slider-next-arrow-'+section
                },
                observeParents: true,
                observer: true
            });

        } else {
            const slides = $(this).find('.swiper-container').data('slides');
            let headerVacanciesSlider = [];
            headerVacanciesSlider[section] = new Swiper('.js-header-menu-stack-slider-'+section, {
                spaceBetween: 35,
                slidesPerView:slides,
                navigation: {
                    nextEl:'.js-header-menu-stack-slider-prev-arrow-'+section,
                    prevEl:'.js-header-menu-stack-slider-next-arrow-'+section
                },
                observeParents: true,
                observer: true
            });
        }
    });

    //todo shalser end-----------------------------------------------------------------------------

    /********** Переключение в навигации в выпадающем меню аутстаф (хедер) *********/

    function changeHeaderBreadcrumbs() {
        let section = $('.header-menu_outstaff-nav li.active-mobile>span').text();
        $('.header-breadcrumbs span.second-item').text(section);

    }

    $('.js-header-menu-outstaff-nav-item').on('click', function () {
        const section = $(this).data('section');
        if (isMobile) {
            $('.js-header-menu-outstaff-nav-item').removeClass('active-mobile');
            $(this).addClass('active-mobile');
        } else {
            $('.js-header-menu-outstaff-nav-item').removeClass('active');
            $(this).addClass('active');
        }
        if ($('.js-header-menu-outstaff-section').hasClass('active')) {
            $('.js-header-menu-outstaff-section').removeClass('active');
        }
        $('.js-header-menu-outstaff-section[data-section=' + section + ']').addClass('active');
        changeHeaderBreadcrumbs();
    });

    //todo shalser start ----------------------------------------------------

    /********** Переключение в навигации в выпадающем меню вакансии (хедер) *********/

    function changeHeaderBreadcrumbsVacancies(){
        let section = $('.header-menu_vacancies-nav li.active-mobile>span').text();
        $('.header-breadcrumbs span.second-item').text(section);
    }

    $('.js-header-menu-vacancies-nav-item').on('click', function () {
        const section = $(this).data('section');
        if (isMobile) {
            $('.js-header-menu-vacancies-nav-item').removeClass('active-mobile');
            $(this).addClass('active-mobile');
        } else {
            $('.js-header-menu-vacancies-nav-item').removeClass('active');
            $(this).addClass('active');
        }
        if($('.js-header-menu-vacancies-section').hasClass('active')) {
            $('.js-header-menu-vacancies-section').removeClass('active');
        }
        $('.js-header-menu-vacancies-section[data-section='+section+']').addClass('active');
        changeHeaderBreadcrumbsVacancies();
    });


//todo shalser end----------------------------------------------------

    /********** Открытие мобильного меню при клике на бургер *********/
    const mobileBurger = $('#mobile-burger');
    mobileBurger.click(function () {
        $('.header-menu').removeClass('active');
        $('.header-menu').hide();
        if (!mobileBurger.hasClass('js-back-to-step-one')) {
            let menuOpened = $(this).hasClass('menu-opened');
            if (menuOpened) {
                $(this).removeClass('menu-opened');
                $('.mobile-menu').slideUp(function () {
                    $('.mesh-header').removeClass('step-two');
                    $('.mesh-header').removeClass('step-three');
                    $('.mesh-nav ul li').addClass('js-to-step-two');
                });
                $('.body-container').css({'height': 'auto', 'overflow-y': 'auto'});
            } else {
                $(this).addClass('menu-opened');
                $('.mobile-menu').slideDown();
                $('.body-container').css({'height': '100vh', 'overflow': 'hidden'});
            }
        } else {
            mobileBurger.removeClass('js-back-to-step-one');
            $('.mesh-header').removeClass('step-two');
            $('.mesh-header').removeClass('step-three');
            $('.mesh-nav ul li').addClass('js-to-step-two');
            $('.mesh-nav ul li').addClass('js-to-step-two');
        }
    });

    $('body').on('click', '.js-to-step-two', function () {
        $('.mesh-header').addClass('step-two');
        $('.js-to-step-two').removeClass('js-to-step-two');
    });

    $('.js-to-step-three').click(function () {
        $('.mesh-header').addClass('step-three');
        $('#mobile-burger').addClass('js-back-to-step-one');
        changeHeaderBreadcrumbs();
        changeHeaderBreadcrumbsVacancies();//todo shalser
    });
    /********* Слайдер модалки с видео *********/
    var portfolioModalPaginationSwiper = new Swiper(".js-portfolio-video-modal-pagination-slider", {
        navigation: {
            nextEl: ".js-portfolio-video-modal-pagination-slider-navigation .arrow-right",
            prevEl: ".js-portfolio-video-modal-pagination-slider-navigation .arrow-left",
        },
        spaceBetween: 4,
        slidesPerView: 6,
        watchSlidesProgress: true,
        observeParents: true,
        observer: true,
        observeChildrens: true
    });
    var portfolioModalSwiper = new Swiper(".js-portfolio-video-modal-slider", {
        thumbs: {
            swiper: portfolioModalPaginationSwiper,
        },
        observeParents: true,
        observer: true,
        observeChildrens: true
    });


    /********** Модалка портфолио **********/

    function openPortfolioModal(section, card) {

        const modal = $('.js-portfolio-video-modal');
        const thisCategory = card.data('cat');
        const modalVideoPart = modal.find('.js-portfolio-video-modal-video-part');
        const cardVideo = card.find('video');
        const modalVideoControls = modalVideoPart.find('.video-controls');
        const videoSrc = cardVideo[0].currentSrc;
        const modalMainSlider = modal.find('.portfolio-video-modal__slider');
        const modalPaginationSlider = modal.find('.js-portfolio-video-modal-pagination-slider');
        var data = $('.js-portfolio-video-modal-card');

        $('.js-portfolio-modal-open[data-cat="' + thisCategory + '"]').each(function (index) {
            modalMainSlider.children('.swiper-wrapper').append(data.clone());
            modalPaginationSlider.children('.swiper-wrapper').append('<div class="swiper-slide"><img src="images/outsource-section-image-1.jpg"></div>');
            let thisSlide = modalMainSlider.find('.js-portfolio-video-modal-card:eq(' + index + ')');
            thisSlide.find('video').children('source').attr('src', $(this).find('video')[0].currentSrc);
        });
        modal.css('display', 'flex');
        $('body').css('overflow-y', 'hidden');
    }

// Вкл и выкл звука на видео
    $('body').on('click', '.js-video-sound', function () {
        const modalVideo = $(this).closest('.js-portfolio-video-modal-video-part').find('video');
        const modalVideoControls = $(this).closest('.video-controls');

        if (!modalVideo.prop('muted')) {
            modalVideoControls.find('.js-video-sound').replaceWith('<svg class="js-video-sound video-sound" width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M14.1513 2.0915C14.5061 2.25469 14.7334 2.60948 14.7334 3.00001V21C14.7334 21.3905 14.5061 21.7453 14.1513 21.9085C13.7965 22.0717 13.3791 22.0134 13.0826 21.7593L6.36347 16H3.7334C3.18111 16 2.7334 15.5523 2.7334 15V9.00001C2.7334 8.44773 3.18111 8.00001 3.7334 8.00001H6.36347L13.0826 2.24076C13.3791 1.9866 13.7965 1.92832 14.1513 2.0915ZM12.7334 5.17423L7.38419 9.75927C7.20295 9.91462 6.97211 10 6.7334 10H4.7334V14H6.7334C6.97211 14 7.20295 14.0854 7.38419 14.2408L12.7334 18.8258V5.17423ZM17.0263 9.29291C17.4168 8.90238 18.05 8.90238 18.4405 9.29291L19.7334 10.5858L21.0263 9.29291C21.4168 8.90238 22.05 8.90238 22.4405 9.29291C22.831 9.68343 22.831 10.3166 22.4405 10.7071L21.1476 12L22.4405 13.2929C22.831 13.6834 22.831 14.3166 22.4405 14.7071C22.05 15.0976 21.4168 15.0976 21.0263 14.7071L19.7334 13.4142L18.4405 14.7071C18.05 15.0976 17.4168 15.0976 17.0263 14.7071C16.6358 14.3166 16.6358 13.6834 17.0263 13.2929L18.3192 12L17.0263 10.7071C16.6358 10.3166 16.6358 9.68343 17.0263 9.29291Z" fill="white"/></svg>');
            modalVideo.prop('muted', true);
        } else {
            modalVideoControls.find('.js-video-sound').replaceWith('<svg class="js-video-sound video-sound" width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M14.1513 2.0915C14.5061 2.25469 14.7334 2.60948 14.7334 3.00001V21C14.7334 21.3905 14.5061 21.7453 14.1513 21.9085C13.7965 22.0717 13.3791 22.0134 13.0826 21.7593L6.36347 16H3.7334C3.18111 16 2.7334 15.5523 2.7334 15V9.00001C2.7334 8.44773 3.18111 8.00001 3.7334 8.00001H6.36347L13.0826 2.24076C13.3791 1.9866 13.7965 1.92832 14.1513 2.0915ZM12.7334 5.17423L7.38419 9.75927C7.20295 9.91462 6.97211 10 6.7334 10H4.7334V14H6.7334C6.97211 14 7.20295 14.0854 7.38419 14.2408L12.7334 18.8258V5.17423ZM18.0263 5.29291C18.4168 4.90238 19.05 4.90238 19.4405 5.29291L19.4412 5.29362L19.442 5.29438L19.4436 5.29605L19.4475 5.29996L19.4574 5.31008C19.4649 5.31784 19.4743 5.32765 19.4854 5.33951C19.5076 5.36322 19.5368 5.39513 19.5719 5.43526C19.6421 5.51549 19.7362 5.62871 19.8459 5.77501C20.0652 6.06748 20.3481 6.49329 20.6278 7.0528C21.189 8.17509 21.7334 9.82725 21.7334 12C21.7334 14.1728 21.189 15.8249 20.6278 16.9472C20.3481 17.5067 20.0652 17.9326 19.8459 18.225C19.7362 18.3713 19.6421 18.4845 19.5719 18.5648C19.5368 18.6049 19.5076 18.6368 19.4854 18.6605C19.4743 18.6724 19.4649 18.6822 19.4574 18.69L19.4475 18.7001L19.4436 18.704L19.442 18.7056L19.4412 18.7064L19.4405 18.7071C19.05 19.0976 18.4168 19.0976 18.0263 18.7071C17.6374 18.3183 17.6358 17.6889 18.0213 17.2979L18.0263 17.2926C18.0334 17.2851 18.0472 17.2701 18.0668 17.2478C18.1059 17.203 18.1681 17.1287 18.2459 17.025C18.4016 16.8175 18.6187 16.4933 18.839 16.0528C19.2778 15.1751 19.7334 13.8272 19.7334 12C19.7334 10.1728 19.2778 8.82494 18.839 7.94723C18.6187 7.50674 18.4016 7.18255 18.2459 6.97501C18.1681 6.87131 18.1059 6.79704 18.0668 6.75227C18.0472 6.72989 18.0334 6.71493 18.0263 6.70739L18.0213 6.70208C17.6358 6.31117 17.6374 5.68176 18.0263 5.29291ZM16.0263 8.29291C16.4168 7.90238 17.05 7.90238 17.4405 8.29291L17.4419 8.29434L17.4435 8.29587L17.4468 8.29923L17.4545 8.30715L17.4742 8.32779C17.4891 8.34369 17.5075 8.36388 17.5289 8.38838C17.5718 8.43736 17.6268 8.50371 17.6896 8.58751C17.8152 8.75498 17.9731 8.99329 18.1278 9.3028C18.439 9.92509 18.7334 10.8273 18.7334 12C18.7334 13.1728 18.439 14.0749 18.1278 14.6972C17.9731 15.0067 17.8152 15.2451 17.6896 15.4125C17.6268 15.4963 17.5718 15.5627 17.5289 15.6116C17.5075 15.6361 17.4891 15.6563 17.4742 15.6722L17.4545 15.6929L17.4468 15.7008L17.4435 15.7042L17.4419 15.7057L17.4412 15.7064C17.0507 16.0969 16.4168 16.0976 16.0263 15.7071C15.6391 15.32 15.6358 14.6943 16.0163 14.303C16.0177 14.3015 16.0202 14.2987 16.0238 14.2946C16.0356 14.2811 16.0587 14.2537 16.0896 14.2125C16.1516 14.13 16.2437 13.9933 16.339 13.8028C16.5278 13.4251 16.7334 12.8272 16.7334 12C16.7334 11.1728 16.5278 10.5749 16.339 10.1972C16.2437 10.0067 16.1516 9.87005 16.0896 9.78751C16.0587 9.74631 16.0356 9.71892 16.0238 9.70539C16.0202 9.70132 16.0177 9.69852 16.0163 9.69701C15.6358 9.30574 15.6391 8.68008 16.0263 8.29291Z" fill="white"/></svg>');
            modalVideo.prop('muted', false);
        }
    });

// Включаем и выключаем видео
    $('body').on('click', '.js-video-play', function () {
        modalVideo = $(this).closest('.js-portfolio-video-modal-video-part').find('video');
        const modalVideoControls = $(this).closest('.video-controls');

        if (modalVideo.get(0).paused) {
            modalVideo.get(0).play();
            modalVideoControls.find('.js-video-play').replaceWith('<svg class="js-video-control js-video-play video-play" width="11" height="13" viewBox="0 0 8 12" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 0C1.55228 0 2 0.447715 2 1V11C2 11.5523 1.55228 12 1 12C0.447715 12 0 11.5523 0 11V1C0 0.447715 0.447715 0 1 0ZM7 0C7.55228 0 8 0.447715 8 1V11C8 11.5523 7.55228 12 7 12C6.44772 12 6 11.5523 6 11V1C6 0.447715 6.44772 0 7 0Z" fill="white"/></svg>');
        } else {
            console.log(modalVideoControls.find('.js-video-play'));
            modalVideo.get(0).pause();
            modalVideoControls.find('.js-video-play').replaceWith('<svg class="js-video-control js-video-play video-play" width="11" height="13" viewBox="0 0 11 13" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.586427 2.16685L0.586426 11.0001C0.586426 12.0856 1.78043 12.7474 2.70094 12.1721L9.76756 7.75545C10.6336 7.21415 10.6336 5.95282 9.76756 5.41152L2.70094 0.994886C1.78044 0.419572 0.586427 1.08135 0.586427 2.16685Z" fill="white"/></svg>');
        }
        // прогресс видео
        modalVideo.off("timeupdate").on("timeupdate", function () {
            // if the video is loaded and duration is known
            console.log(this.duration);
            if (!isNaN(this.duration)) {
                let percent_complete = this.currentTime / this.duration * 100;
                modalVideoControls.find('.js-video-progress-bar').css('width', percent_complete + '%');
            }
        });
        timeUpdateEvent = true;

    });
// // Сбрасываем прогресс бар при смене слайда
// portfolioModalSwiper.on('slideChange',function(){
//   $('.js-video-progress-bar').css('width','0%');
// });

    function closePortfolioModal() {
        const modal = $('.js-portfolio-video-modal');
        const modalVideo = $('.js-portfolio-video-modal video');
        const modalMainSlider = modal.find('.portfolio-video-modal__slider');
        const modalPaginationSlider = modal.find('.js-portfolio-video-modal-pagination-slider');
        modalMainSlider.children('.swiper-wrapper').empty();
        modalPaginationSlider.children('.swiper-wrapper').empty();
        modalVideo.get(0).pause();
        modal.hide();
        $('body').css('overflow-y', 'auto');
    }

    $('body').on('click', '.js-portfolio-video-modal-close', function () {
        closePortfolioModal();
    });

    $('body').on('click', '.js-portfolio-modal-open', function () {
        const section = $(this).closest('.js-portfolio-section');
        const card = $(this);
        openPortfolioModal(section, card);
    });
    /********** Скролл блока целей на странице портфолио  **********/
    var leftPos = $(this).scrollLeft();
    $('.js-mesh-outsource-section-cards-container').on('wheel', function (e) {
        e.preventDefault(); //отменяем вертиклаьный скролл
        const containerScrollWidth = $(this).get(0).scrollWidth - $(this).width(); // получаем максимальное возможное значение скролла
        let scroll;
        let step = e.originalEvent.wheelDeltaY;

        // Определяем в какую сторону идёт скролл
        if (step < 0) {
            scroll = 'right';
        } else {
            scroll = 'left';
        }

        // Проверяем начало и конец контейнера в зависимости от направления скролла
        if (scroll == 'right') {

            if (leftPos < containerScrollWidth) {
                // Проверяем, чтобы последний скролл не ушёл за пределы контейнера
                if (-(step - leftPos) > containerScrollWidth) {
                    step = -(containerScrollWidth - leftPos);
                    leftPos = leftPos - step;
                } else {
                    leftPos = leftPos - step;
                }

            }

        } else if (scroll == 'left') {
            if (leftPos > 0) {
                // Проверяем, чтобы последний скролл не ушёл за пределы контейнера
                if (leftPos - step < 0) {
                    step = leftPos;
                    leftPos = leftPos - step;
                } else {
                    leftPos = leftPos - step;
                }
            }
        }

        $(this).stop().animate({scrollLeft: leftPos}, 100);
    });

// Включение видео при наведении

    $('.js-portfolio-modal-open').mouseenter(function () {
        const cardVideo = $(this).find('video');
        cardVideo.get(0).play();
    });

    $('.js-portfolio-modal-open').mouseleave(function () {
        const cardVideo = $(this).find('video');
        cardVideo.get(0).pause();
        cardVideo.get(0).currentTime = 0;
    });


});
